﻿-- DROP FUNCTION so_proforma_jr(numeric)
CREATE OR REPLACE FUNCTION so_proforma_jr(
    pinstance numeric)
    RETURNS void AS
$BODY$
DECLARE
/**
p--- ERP 帶入的參數
v--- procedure 運算用的變數 
形式發票(Proforma invoice)

https://wiki.mbalib.com/zh-tw/%E5%BD%A2%E5%BC%8F%E5%8F%91%E7%A5%A8
*/
ResultStr VARCHAR (200);
roleaccesslevelwin VARCHAR (200);
sql VARCHAR (2000);
sqldel VARCHAR (2000);

p RECORD;
r RECORD;

p_DateAcct DATE;
p_DateAcct_TO DATE;
p_DateStep DATE;
p_DateFrom DATE;

p_Record_ID NUMERIC(10) := 0;
p_AD_User_ID NUMERIC(10) := 0;
p_User_ID NUMERIC(10) := 0;
p_AD_Client_ID NUMERIC(10) := 0;
p_AD_Org_ID NUMERIC(10) := 0;
p_AD_Org_ID_TO NUMERIC(10) := 0;
p_IsSOTrx VARCHAR (1) := 'Y';
p_C_DocType_ID NUMERIC(10) := 0;
p_C_DocType_ID_2 NUMERIC(10) := 0;

p_C_BPartner_Value VARCHAR (40) := 0;
p_C_DocType_Value VARCHAR (40) := 0;
p_C_BPartner_Value_TO VARCHAR (40) := '';
p_C_DocType_Value_TO VARCHAR (40) := '';
p_PNType VARCHAR (40) := 'N'; -- V , N , VN

p_MovementDate DATE;
p_MovementDate_TO DATE;
p_DateInvoiced DATE;
p_DateInvoiced_TO DATE;

v_message VARCHAR (400) := '';
v_documentno VARCHAR (30) := '';
v_count NUMERIC(10) := 0;
v_NextNo NUMERIC(10) := 0;
v_PN VARCHAR (40) := '';

BEGIN


v_message :='程式開始:: 更新[呼叫程序]紀錄檔...開始執行時間(created)..程序執行中(isprocessing)..';
--IF pinstance > 0 THEN
BEGIN
ResultStr := 'PInstanceNotFound';
UPDATE adempiere.ad_pinstance
SET created = SYSDATE,
isprocessing = 'Y',
reslut = 0
WHERE ad_pinstance_id = pinstance_id;
COMMIT;
EXCEPTION
WHEN OTHERS THEN NULL;

END;

v_message :='Parameter loading';

FOR p IN
(SELECT pi.record_id, pi.ad_client_id,pi.ad_org_id, pi.ad_user_id, 
pp.parametername,
pp.p_string, pp.p_number, pp.p_date,
pp.p_string_TO, pp.p_number_TO, pp.p_date_tO
FROM adempiere.ad_pinstance pi
LEFT OUTER JOIN adempiere.ad_pinstance_para pp ON(pi.ad_pinstance_id = pp.ad_pinstance_id)
WHERE pi.ad_pinstance_id = pinstance
ORDER BY pp.SeqNo)
LOOP
p_Record_ID := p.record_id;
p_User_ID := p.AD_User_id;
p_AD_Client_ID := p.AD_Client_ID;
p_AD_Org_ID := p.AD_Org_ID;

  IF p.parametername = 'DateAcct' THEN p_DateAcct = p.p_date;
     p_DateAcct_TO = p.p_date_to;
  ELSIF p.parametername = 'AD_Org_ID' THEN p_AD_Org_ID = p.p_number;
     p_AD_Org_ID_TO = p.p_number_to;
  ELSIF p.parametername = 'PNType' THEN p_PNType = p.p_string;
  ELSIF p.parametername = 'AD_User_ID' THEN p_AD_User_ID = p.p_number;
  END IF;

END LOOP;



IF p_User_ID IS NULL THEN 
   p_User_ID := 0; 
END IF;

-- 測試用
IF pinstance = 1000000 THEN

p_DateAcct := '2018-01-01';
p_DateAcct_TO := '2018-01-19';
---p_AD_User_ID := 1000784;
--  C_Order_ID=1005392
p_Record_ID = 1005392;
END IF;

v_message :='Start Process';


TRUNCATE T_ProformaInovice_jr;


/* 保留字需要雙引號  */

/* 測試 CODE

select  so_proforma_jr(1000000)
select * from T_ProformaInovice_jr
select  ad_pinstance_id, errormsg from  adempiere.ad_pinstance where ad_pinstance_id = 1000000
*/

FOR r IN (
  -- drop table T_ProformaInovice_jr
  -- create table T_ProformaInovice_jr as 
  SELECT
	--ORG BP Information
	1000000::numeric(10) as AD_PInstance_ID 
	,(select name from c_bpartner where ad_orgbp_id = o.ad_org_id) OrgBPName
	,(select taxid from c_bpartner where ad_orgbp_id = o.ad_org_id) OrgBPTaxID
	
	, (select address1 from c_location l 
		inner join C_BPartner_Location bl on l.c_location_id = bl.c_location_id
		inner join C_BPartner b on b.c_bpartner_id = bl.c_bpartner_id
		where exists (select 1 from c_bpartner where ad_orgbp_id = o.ad_org_id and c_bpartner_id = b.c_bpartner_id  )  and l.isactive = 'Y' limit 1) OrgBPAddress
	, (select phone from c_location l 
		inner join C_BPartner_Location bl on l.c_location_id = bl.c_location_id
		inner join C_BPartner b on b.c_bpartner_id = bl.c_bpartner_id
		where exists (select 1 from c_bpartner where ad_orgbp_id = o.ad_org_id and c_bpartner_id = b.c_bpartner_id  )  and l.isactive = 'Y' limit 1) OrgBPPhone		
	, (select name from ad_user where ad_user_id = o.SalesRep_ID ) SalesRepresentative
      , (select email from ad_user where ad_user_id = o.SalesRep_ID ) SalesRepresentativeEmail


	-- Customer Information
	,(select name from c_bpartner where c_bpartner_id = o.c_bpartner_id) BPName
	,(select taxid from c_bpartner where c_bpartner_id = o.c_bpartner_id) BPTaxID
	
	, (select address1 from c_location l 
		inner join C_BPartner_Location bl on l.c_location_id = bl.c_location_id
		inner join C_BPartner b on b.c_bpartner_id = bl.c_bpartner_id
		where exists (select 1 from c_bpartner where  c_bpartner_id = o.c_bpartner_id  )  and l.isactive = 'Y' limit 1) BPAddress
	, (select phone from c_location l 
		inner join C_BPartner_Location bl on l.c_location_id = bl.c_location_id
		inner join C_BPartner b on b.c_bpartner_id = bl.c_bpartner_id
		where exists (select 1 from c_bpartner where  c_bpartner_id = o.c_bpartner_id )  and l.isactive = 'Y' limit 1) BPPhone	
	, (select name from ad_user where ad_user_id = o.AD_User_ID ) bpContactUser
	-- Order Information
	, (select PrintName from c_doctype where c_doctype_id = o.c_doctypetarget_id) doctypeName
	, pl.IsTaxIncluded
	, (select iso_code from c_currency where c_currency_id = pl.c_currency_id) currency
	, o.grandtotal
	, (select TaxAmt from C_OrderTax where C_Order_id = o.C_Order_ID) TaxAmt
	, (select TaxBaseAmt from C_OrderTax where C_Order_id = o.C_Order_ID) TaxBaseAmt
	,o.DateOrdered
	,o.DatePromised
	, (select name from m_product where m_product_id = ol.m_product_id) pn
	, (select name from c_tax where c_tax_id = ol.c_tax_id ) taxrate
	,o.description
	,o.documentno
	,ol.m_product_id
	,ol.PriceEntered
	,ol.QtyEntered
	,ol.LineNetAmt
	,ol.Description LineDescription
	
	from c_order o 
	inner join ad_org org on o.ad_org_id = org.ad_org_id
	inner join c_orderline ol on o.c_order_id = ol.c_order_id 
	inner join M_PriceList pl on o.M_PriceList_ID = pl.M_PriceList_ID
	--where o.c_order_id  = 1005392
	where o.c_order_id  = p_Record_ID
  -- select something

)LOOP
v_message :='LOOP process';
if p_PNType = 'V' then 

	select value into v_PN from m_product where m_product_id = r.m_product_id;

elsif p_PNType='VN' then
       select value || ' ' || name into v_PN from m_product where m_product_id = r.m_product_id;
else 
    -- default = N
    v_PN := r.pn;
	
end if;
-- insert into .....temp table

INSERT INTO t_proformainovice_jr(
            ad_pinstance_id, orgbpname, orgbptaxid, orgbpaddress, orgbpphone, 
            salesrepresentative, bpname, bptaxid, bpaddress, bpphone, bpcontactuser, 
            istaxincluded, currency, grandtotal, taxamt, taxbaseamt, dateordered, 
            datepromised, pn, taxrate, description, m_product_id, priceentered, 
            qtyentered, linenetamt, linedescription,SalesRepresentativeEmail,doctypeName,documentno)
    VALUES (pinstance, r.orgbpname, r.orgbptaxid, r.orgbpaddress, r.orgbpphone, 
            r.salesrepresentative, r.bpname, r.bptaxid, r.bpaddress, r.bpphone, r.bpcontactuser, 
            r.istaxincluded, r.currency, r.grandtotal, r.taxamt, r.taxbaseamt, r.dateordered, 
            r.datepromised, r.pn, r.taxrate, r.description, r.m_product_id, r.priceentered, 
            r.qtyentered, r.linenetamt, r.linedescription,r.SalesRepresentativeEmail,r.doctypeName,r.documentno);

	
END LOOP;
-- 3) RETURN
--RETURN QUERY SELECT * From t_fact_detail_ireport yy where yy.ad_pinstance_id = pinstance order by line;
-- select * from c_bpartner  where C_BPartner_ID=1000562

v_message :='END Process';

IF pinstance > 0 THEN
BEGIN
UPDATE adempiere.ad_pinstance
SET updated = now(),
isprocessing = 'N',
result = 1,
errormsg = v_message
WHERE ad_pinstance_id = pinstance;
-- COMMIT;
EXCEPTION
WHEN OTHERS THEN NULL;
END;
END IF;

EXCEPTION

WHEN OTHERS THEN
--sqlins := 'INSERT INTO adempiere.t_an_shipment (ad_pinstance_id, t_an_shipment_id, bp_name) VALUES($1, $2, $3)';
--EXECUTE sqlins USING pinstance, 9, v_message;
--v_message :='例外錯誤。。。';
UPDATE adempiere.ad_pinstance
SET updated = now(),
isprocessing = 'N',
result = 0,
errormsg = v_message
WHERE ad_pinstance_id = pinstance;
-- COMMIT;

END; 

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION so_proforma_jr(numeric)
  OWNER TO adempiere;

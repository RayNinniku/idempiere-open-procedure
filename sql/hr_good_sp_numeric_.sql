CREATE OR REPLACE FUNCTION hr_good_sp(pinstance numeric)
  RETURNS void AS
$BODY$
DECLARE

ResultStr VARCHAR (200);
roleaccesslevelwin VARCHAR (200);
sql VARCHAR (2000);


p RECORD;
--r RECORD;
v_message text;
p_DateAcct DATE;
p_DateAcct_TO DATE;
p_User_ID NUMERIC(10) := 0;
p_Record_ID NUMERIC(10) := 0;
p_AD_User_ID NUMERIC(10) := 0;
p_AD_Client_ID NUMERIC(10) := 0;
p_AD_Org_ID NUMERIC(10) := 0;
BEGIN

IF pinstance is null THEN 
	pinstance:=0;
END IF;

v_message :='Starting ..';

BEGIN
ResultStr := 'PInstanceNotFound';
UPDATE adempiere.ad_pinstance
SET created = SYSDATE,
isprocessing = 'Y',
reslut = 0
WHERE ad_pinstance_id = pinstance_id;
COMMIT;
EXCEPTION
WHEN OTHERS THEN NULL;

END;

v_message :='Loading Parameter';

FOR p IN
(SELECT pi.record_id, pi.ad_client_id,pi.ad_org_id, pi.ad_user_id, 
pp.parametername,
pp.p_string, pp.p_number, pp.p_date,
pp.p_string_TO, pp.p_number_TO, pp.p_date_tO
FROM adempiere.ad_pinstance pi
LEFT OUTER JOIN adempiere.ad_pinstance_para pp ON(pi.ad_pinstance_id = pp.ad_pinstance_id)
WHERE pi.ad_pinstance_id = pinstance
ORDER BY pp.SeqNo)
LOOP
p_Record_ID := p.record_id;
p_User_ID := p.AD_User_id;
p_AD_Client_ID := p.AD_Client_ID;
p_AD_Org_ID := p.AD_Org_ID;

  IF p.parametername = 'DateAcct' THEN p_DateAcct = p.p_date;
     p_DateAcct_TO = p.p_date_to;
  ELSIF p.parametername = 'AD_User_ID' THEN p_AD_User_ID = p.p_number;

  END IF;

END LOOP;


IF p_User_ID IS NULL THEN 
   p_User_ID := 0; 
END IF;

IF pinstance = 1000000 THEN
p_Record_ID = 100;
END IF;

v_message :='Start Process';

 update ad_user set counter = (coalesce(counter,0) + 1 ) where ad_user_id = p_Record_ID;


v_message :='END Process';

IF pinstance > 0 THEN
BEGIN
UPDATE adempiere.ad_pinstance
SET updated = now(),
isprocessing = 'N',
result = 1,
errormsg = v_message
WHERE ad_pinstance_id = pinstance;
-- COMMIT;
EXCEPTION
WHEN OTHERS THEN NULL;
END;
END IF;

EXCEPTION

WHEN OTHERS THEN
--sqlins := 'INSERT INTO adempiere.t_an_shipment (ad_pinstance_id, t_an_shipment_id, bp_name) VALUES($1, $2, $3)';
--EXECUTE sqlins USING pinstance, 9, v_message;
--v_message :='例外錯誤。。。';
UPDATE adempiere.ad_pinstance
SET updated = now(),
isprocessing = 'N',
result = 0,
errormsg = v_message
WHERE ad_pinstance_id = pinstance;
-- COMMIT;

END; 

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION hr_good_sp(numeric)
  OWNER TO adempiere;

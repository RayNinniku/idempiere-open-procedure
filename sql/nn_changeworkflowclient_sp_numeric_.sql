CREATE OR REPLACE FUNCTION nn_changeworkflowclient_sp(pinstance numeric)
  RETURNS void AS
$BODY$
DECLARE

ResultStr VARCHAR (200);
roleaccesslevelwin VARCHAR (200);
sql text;

p RECORD;
r RECORD;
s RECORD;
inv RECORD;

p_Record_ID NUMERIC(10) := 0;
p_AD_Client_ID NUMERIC(10) := 0;
p_AD_Org_ID NUMERIC(10) := 0;
p_AD_User_ID NUMERIC(10) := 0;
p_User_ID NUMERIC(10) := 0;
p_C_BPartner_ID NUMERIC(10) := 0;
p_AD_Workflow_ID NUMERIC(10) := 0;


p_MovementDate DATE;
p_MovementDate_TO DATE;
v_message VARCHAR (400) := '';
v_NextNo NUMERIC(10) := 0;

BEGIN

IF pinstance is null THEN pinstance:=0;
END IF;

v_message :='程式開始:: 更新[呼叫程序]紀錄檔...開始執行時間(created)..程序執行中(isprocessing)..';
--IF pinstance > 0 THEN
BEGIN
ResultStr := 'PInstanceNotFound';
UPDATE adempiere.ad_pinstance
SET created = SYSDATE,
isprocessing = 'Y',
reslut = 0
WHERE ad_pinstance_id = pinstance_id;
COMMIT;
EXCEPTION
WHEN OTHERS THEN NULL;

END;

v_message :='未出入明細表, 由 c_order';

FOR p IN
  (SELECT pi.record_id,
          pi.ad_client_id,
          pi.ad_org_id,
          pi.ad_user_id,
          pp.parametername,
          pp.p_string,
          pp.p_number,
          pp.p_date,
          pp.p_string_TO,
          pp.p_number_TO,
          pp.p_date_tO
   FROM adempiere.ad_pinstance pi
   LEFT OUTER JOIN adempiere.ad_pinstance_para pp ON(pi.ad_pinstance_id = pp.ad_pinstance_id)
   WHERE pi.ad_pinstance_id = pinstance
   ORDER BY pp.SeqNo) 
LOOP 
  p_Record_ID := p.record_id;
  p_User_ID := p.AD_User_id;
  p_AD_Client_ID := p.AD_Client_ID;
  p_AD_Org_ID := p.AD_Org_ID;

  IF p.parametername = 'AD_Client_ID' THEN p_AD_Client_ID = p.p_number;
  ELSIF p.parametername = 'AD_Workflow_ID' THEN p_AD_Workflow_ID = p.p_number;
  ELSIF p.parametername = 'AD_User_ID' THEN p_AD_User_ID = p.p_number;
  END IF;

 END LOOP;


IF p_User_ID IS NULL THEN 
   p_User_ID := 0; 
END IF;

IF pinstance = 0 THEN
p_MovementDate := date_trunc('YEAR', now());
p_MovementDate_TO:= date_trunc('DAY', now());
END IF;

v_message :='START';

update AD_Workflow set ad_client_id = p_AD_Client_ID where AD_Workflow_ID= p_AD_Workflow_ID ;
update  AD_Workflow_Trl set ad_client_id = p_AD_Client_ID where AD_Workflow_ID= p_AD_Workflow_ID ;
update  AD_Workflow_Access set ad_client_id = p_AD_Client_ID where AD_Workflow_ID= p_AD_Workflow_ID ;


-- node
update  AD_WF_Node set ad_client_id = p_AD_Client_ID where AD_Workflow_ID= p_AD_Workflow_ID ;
update AD_WF_Node_Para set  ad_client_id = p_AD_Client_ID
where AD_WF_Node_ID in (select AD_WF_Node_ID from AD_WF_Node where AD_Workflow_ID=p_AD_Workflow_ID );

update AD_WF_Node_Trl set  ad_client_id = p_AD_Client_ID
where AD_WF_Node_ID in (select AD_WF_Node_ID from AD_WF_Node where AD_Workflow_ID=p_AD_Workflow_ID );

update AD_WF_NodeNext set  ad_client_id = p_AD_Client_ID
where AD_WF_Node_ID in (select AD_WF_Node_ID from AD_WF_Node where AD_Workflow_ID=p_AD_Workflow_ID );

update AD_WF_NextCondition set  ad_client_id = p_AD_Client_ID where AD_WF_NodeNext_ID in 
(
select AD_WF_NodeNext_ID from AD_WF_NodeNext where AD_WF_Node_ID in (select AD_WF_Node_ID from AD_WF_Node where AD_Workflow_ID=p_AD_Workflow_ID ) 
);

v_message :='DONE';
v_message :=  concat('修改完成');
IF pinstance > 0 THEN
BEGIN
UPDATE adempiere.ad_pinstance
SET updated = now(),
isprocessing = 'N',
result = 1,
errormsg = v_message
WHERE ad_pinstance_id = pinstance;
-- COMMIT;
EXCEPTION
WHEN OTHERS THEN NULL;
END;
END IF;


EXCEPTION

WHEN OTHERS THEN
--v_message :='例外錯誤。。。';
UPDATE adempiere.ad_pinstance
SET updated = now(),
isprocessing = 'N',
result = 0,
errormsg = v_message
WHERE ad_pinstance_id = pinstance;
-- COMMIT;

END; 

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION nn_changeworkflowclient_sp(numeric)
  OWNER TO adempiere;
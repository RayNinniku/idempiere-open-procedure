#!/bin/bash
# Ninniku Consulting Ray
#
sudo mkdir -p /usr/local/java
wget https://s3-ap-northeast-1.amazonaws.com/aierp/iDempiereTaiwan-0126/openjdk-11%2B28_linux-x64_bin.tar.gz
sudo cp -r openjdk-11+28_linux-x64_bin.tar.gz /usr/local/java/
cd /usr/local/java
sudo tar xvzf openjdk-11+28_linux-x64_bin.tar.gz

sudo echo "JAVA_HOME=/usr/local/java/jdk-11 " | tee -a /etc/profile
sudo echo "PATH=$PATH:$JAVA_HOME/bin " | tee -a /etc/profile
sudo echo "export JAVA_HOME " | tee -a /etc/profile
sudo echo "export PATH " | tee -a /etc/profile

sudo update-alternatives --install "/usr/bin/java" "java" "/usr/local/java/jdk-11/bin/java" 1
sudo update-alternatives --install "/usr/bin/javac" "javac" "/usr/local/java/jdk-11/bin/javac" 1
#sudo update-alternatives --install "/usr/bin/javaws" "javaws" "/usr/local/java/jdk-11/bin/javaws" 1
sudo update-alternatives --set java /usr/local/java/jdk-11/bin/java
sudo update-alternatives --set javac /usr/local/java/jdk-11/bin/javac
#sudo update-alternatives --set javaws /usr/local/java/jdk-11/bin/javaws
echo "Installation Completed!\n"
echo "Please excute [CODE] source /etc/profile [CODE] to enable Java"

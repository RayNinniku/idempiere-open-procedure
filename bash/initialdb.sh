#!/bin/sh
# Ray / Ninniu Consuting
# ray@ninniku.tw
# How to: Under utils directory to execute bash initialdb.sh
STARTTIME="$(date)"

. ./myEnvironment.sh Server

PGPASSWORD=$ADEMPIERE_DB_PASSWORD
export PGPASSWORD
echo -------------------------------------
echo Recreate user and database
echo -------------------------------------
echo It will delete your current Database $ADEMPIERE_DB_NAME and Database User $ADEMPIERE_DB_USER ...
echo Press enter to continue ...
read in

echo "Drop DB .... " 
dropdb -h $ADEMPIERE_DB_SERVER -p $ADEMPIERE_DB_PORT -U $ADEMPIERE_DB_USER $ADEMPIERE_DB_NAME

PGPASSWORD=$ADEMPIERE_DB_SYSTEM
export PGPASSWORD

dropuser -h $ADEMPIERE_DB_SERVER -p $ADEMPIERE_DB_PORT -U postgres  $ADEMPIERE_DB_USER


ADEMPIERE_CREATE_ROLE_SQL="CREATE ROLE $ADEMPIERE_DB_USER NOSUPERUSER LOGIN PASSWORD '$ADEMPIERE_DB_PASSWORD'"
psql -h $ADEMPIERE_DB_SERVER -p $ADEMPIERE_DB_PORT -U postgres -c "$ADEMPIERE_CREATE_ROLE_SQL"
ADEMPIERE_CREATE_ROLE_SQL=

#PGPASSWORD=$ADEMPIERE_DB_PASSWORD
#export PGPASSWORD
createdb -h $ADEMPIERE_DB_SERVER -p $ADEMPIERE_DB_PORT -E UNICODE -O postgres -U postgres $ADEMPIERE_DB_NAME
psql -h $ADEMPIERE_DB_SERVER -p $ADEMPIERE_DB_PORT -d $ADEMPIERE_DB_NAME -U postgres -c "GRANT postgres TO adempiere"

echo -------------------------------------
echo Create Database, Schema , uuid-ossp
echo -------------------------------------

psql -h $ADEMPIERE_DB_SERVER -p $ADEMPIERE_DB_PORT -d $ADEMPIERE_DB_NAME -U postgres -c "ALTER DATABASE $ADEMPIERE_DB_NAME OWNER TO $ADEMPIERE_DB_USER;"

PGPASSWORD=$ADEMPIERE_DB_PASSWORD
export PGPASSWORD
psql -h $ADEMPIERE_DB_SERVER -p $ADEMPIERE_DB_PORT -d $ADEMPIERE_DB_NAME -U $ADEMPIERE_DB_USER -c "CREATE SCHEMA adempiere;"
psql -h $ADEMPIERE_DB_SERVER -p $ADEMPIERE_DB_PORT -d $ADEMPIERE_DB_NAME -U $ADEMPIERE_DB_USER -c "ALTER SCHEMA adempiere OWNER TO $ADEMPIERE_DB_USER;"
psql -h $ADEMPIERE_DB_SERVER -p $ADEMPIERE_DB_PORT -d $ADEMPIERE_DB_NAME -U $ADEMPIERE_DB_USER -c "ALTER SCHEMA adempiere OWNER TO postgres;"


PGPASSWORD=$ADEMPIERE_DB_SYSTEM
export PGPASSWORD
psql -h $ADEMPIERE_DB_SERVER -p $ADEMPIERE_DB_PORT -d $ADEMPIERE_DB_NAME -U postgres -c "CREATE EXTENSION IF NOT EXISTS \"uuid-ossp\" WITH SCHEMA adempiere;"

#CREATE EXTENSION IF NOT EXISTS "uuid-ossp" WITH SCHEMA adempiere;
#psql -h $ADEMPIERE_DB_SERVER -p $ADEMPIERE_DB_PORT -d $ADEMPIERE_DB_NAME -U postgres -f uuid.dmp

psql -h $ADEMPIERE_DB_SERVER -p $ADEMPIERE_DB_PORT -d $ADEMPIERE_DB_NAME -U postgres -c "ALTER SCHEMA adempiere OWNER TO $ADEMPIERE_DB_USER;"

echo -------------------------------------
echo Import Data base
echo -------------------------------------

PGPASSWORD=$ADEMPIERE_DB_PASSWORD
export PGPASSWORD
ADEMPIERE_ALTER_ROLE_SQL="ALTER ROLE $ADEMPIERE_DB_USER SET search_path TO adempiere, pg_catalog"
psql -h $ADEMPIERE_DB_SERVER -p $ADEMPIERE_DB_PORT -d $ADEMPIERE_DB_NAME -U $ADEMPIERE_DB_USER -c "$ADEMPIERE_ALTER_ROLE_SQL"

echo "Uncompress Database"
cd $IDEMPIERE_HOME/data/seed/
jar xvf Adempiere_pg.jar 

psql -h $ADEMPIERE_DB_SERVER -p $ADEMPIERE_DB_PORT -d $ADEMPIERE_DB_NAME -U $ADEMPIERE_DB_USER -f Adempiere_pg.dmp
rm  Adempiere_pg.dmp
PGPASSWORD=
export PGPASSWORD
cd $IDEMPIERE_HOME/utils/
echo "from:  $STARTTIME"
echo "End:" "$(date)"

